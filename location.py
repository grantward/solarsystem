"""Provides coordinate-based location."""

import math
import weakref


class Point:
    """Singular point in space, reference to central object."""
    def __init__(self, x, y, z):
        """x, y, z, distance in meters. Negative and positive values
        are directly opposed to each other on a line.
        """
        self.x = x
        self.y = y
        self.z = z


class Location:
    """Object's location and associated methods."""
    def __init__(self, point, updated):
        """
        point -> Point
        updated -> float (time in seconds since epoch)
        """
        self.point = point
        self.updated = updated

    def update(self, point, updated=None):
        """
        point -> Point, new location
        updated -> float, time updated, or now)
        """
        self.point = point
        self.updated = updated if updated else time.time()

    def last_updated(self, use_time=None):
        """
        Time in seconds since last update
        """
        return use_time if use_time else time.time() - self.updated


class SpaceObject:
    """Object in space with mass and potential to move."""
    def __init__(self, mass, location, orbit=None):
        """
        mass -> float (kg)
        location -> Location
        orbit -> SpaceObject orbiting
        """
        self.mass = mass
        self.location = location
        self.orbit = orbit

    @property
    def position(self):
        """Returns point of current position."""
        current_position = self.location.point
        time_elapsed = self.location.last_updated()
        # TODO: left off here


class Orbit:
    """Describes the orbit path around object."""
    def __init__(self, massive, small, distance, x_arc, y_arc):
        """
        massive -> SpaceObject
        small -> SpaceObject
        distance -> float (m) center to center distance (radius)
        x_arc -> one component of plane of orbit
        y_arc -> other component of plane of orbit
        """
        self._massive = weakref.ref(massive)
        self._small = weakref.ref(small)
        self.distance = distance
        self.x_arc = x_arc
        self.y_arc = y_arc

    @property
    def massive(self):
        """Retrieve massive object"""
        return self._massive()

    @property
    def small(self):
        """Retrieve smaller object"""
        return self._small()

    @property
    def velocity(self):
        """Return the instantaneous velocity of the smaller object."""
        # m3/kg*s2
        grav_constant = 6.67430e-11
        grav_parameter = grav_constant * self.massive.mass
        return math.sqrt(grav_parameter / self.distance)


class MassiveObject(SpaceObject):
    """Object in space with large mass, ex. Planet, Moon, Sun"""


class SmallObject(SpaceObject):
    """Object in space with small mass ex. ship, asteroid"""
